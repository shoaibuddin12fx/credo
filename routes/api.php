<?php

use App\Http\Controllers\API\HelperController;
use App\Http\Controllers\API\StudentAffairsController;
use App\Http\Controllers\API\StudentManagementController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\MicroPerformanceAcademicsDepartmentController;
use App\Http\Controllers\API\MicroPerformanceCounsellingDepartmentController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'students', 'as' => 'students.'], function () {
    Route::get('index', [StudentManagementController::class, 'index']);
    Route::get('show/{id}', [StudentManagementController::class, 'show']);
    Route::get('profile/{id}', [StudentManagementController::class, 'profile']);
    Route::post('profile/setpicture', [StudentManagementController::class, 'setPicture']);
    Route::post('profile/update', [StudentManagementController::class, 'update']);
    Route::post('profile/setcolor', [StudentManagementController::class, 'setColor']);
});

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::post('login', [UserController::class, 'login']);
    Route::get('users/{role}', [UserController::class, 'users']);
});

Route::group(['prefix' => 'helper', 'as' => 'helper.'], function () {
    Route::get('subjects', [HelperController::class, 'subjects']);
    Route::get('student_classes', [HelperController::class, 'studentClasses']);
    Route::get('sections', [HelperController::class, 'sections']);
});

Route::group(['prefix' => 'student_affairs', 'as' => 'student_affairs.'], function () {
    Route::get('index', [StudentAffairsController::class, 'index']);
    Route::post('store', [StudentAffairsController::class, 'store']);
    Route::get('show/{id}', [StudentAffairsController::class, 'show']);
    Route::post('update/{id}', [StudentAffairsController::class, 'update']);
    Route::get('student_affairs_by_student_id/{studentId}', [StudentAffairsController::class, 'studentAffairsData']);
    Route::post('approve-reject/{id}', [StudentAffairsController::class, 'ApproveReject']);
});

Route::group(['prefix' => 'micro_performance_counselling_department', 'as' => 'micro_performance_counselling_department.'], function () {
    Route::get('index', [MicroPerformanceCounsellingDepartmentController::class, 'index']);
    Route::post('store', [MicroPerformanceCounsellingDepartmentController::class, 'store']);
    Route::get('show/{id}', [MicroPerformanceCounsellingDepartmentController::class, 'show']);
    Route::post('update/{id}', [MicroPerformanceCounsellingDepartmentController::class, 'update']);
    Route::get('counselling_department_by_student_id/{studentId}', [MicroPerformanceCounsellingDepartmentController::class, 'getRecordsByStudentId']);
    Route::post('approve-reject/{id}', [MicroPerformanceCounsellingDepartmentController::class, 'ApproveReject']);
});

Route::group(['prefix' => 'micro_performance_academics_department', 'as' => 'micro_performance_academics_department.'], function () {
    Route::get('index', [MicroPerformanceAcademicsDepartmentController::class, 'index']);
    Route::post('store', [MicroPerformanceAcademicsDepartmentController::class, 'store']);
    Route::get('show/{id}', [MicroPerformanceAcademicsDepartmentController::class, 'show']);
    Route::post('update/{id}', [MicroPerformanceAcademicsDepartmentController::class, 'update']);
    Route::get('academics_department_by_student_id/{studentId}', [MicroPerformanceAcademicsDepartmentController::class, 'getRecordsByStudentId']);
    Route::post('approve-reject/{id}', [MicroPerformanceAcademicsDepartmentController::class, 'ApproveReject']);
});
