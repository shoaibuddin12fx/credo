<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentClass;
use App\Models\StudentSection;
use App\Models\StudentSubject;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\User;

class HelperController extends Controller
{
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
                //$data[] =  $row;
            }
            fclose($handle);
        }

        return $data;
    }

    public function importCsv()
    {
        $file = public_path('For-App-Developers-Student-Subject-Section-Teacher-Data.csv');

        $customerArr = $this->csvToArray($file);
        $result = array();
        foreach ($customerArr as $element) {
            $result[$element['ID']][] = $element;
        }
        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
        // for ($i = 0; $i < count($result); $i++) {
        //     echo '<pre>';
        //     print_r($result[$i]);
        //     echo '</pre>';
        //   //  User::firstOrCreate($customerArr[$i]);
        // }
        $subjects =[];
        $subjectArr =[];
        $classes =[];
        $classesArr =[];
        $sections =[];
        $sectionsArr =[];
        foreach ($result as $key => $value) {
                // echo '<pre>';
                // print_r($key );
                // print_r($value[0]['Name']);
                // print_r($value[0]);
                // print_r(count($value));
                foreach($value as $k=> $val) {
                   if(!in_array($val['Subject'],$subjects)){
                    $subjects[] =$val['Subject'];
                    $subjectArr[] =[
                        'name'=>$val['Subject']
                    ];
                   }

                   if(!in_array($val['Class'],$classes)){
                    $classes[] =$val['Class'];
                    $classesArr[] =[
                        'name'=>$val['Class']
                    ];
                   }

                   if(!in_array($val['Section'],$sections)){
                    $sections[] =$val['Section'];
                    $sectionsArr[] =[
                        'name'=>$val['Section']
                    ];
                   }

                }

                // echo '</pre>';
        }
        Subject::truncate();
        Subject::insert($subjectArr);
        StudentClass::truncate();
        StudentClass::insert($classesArr);
        StudentSection::truncate();
        StudentSection::insert($sectionsArr);
        // echo '<pre>';
        // print_r($classesArr);
        // echo '</pre>';
        Student::truncate();
        StudentSubject::truncate();
        foreach ($result as $key => $value) {
            $name = explode(" ",$value[0]['Name']);
            $class = StudentClass::where('name',$value[0]['Class'])->first();
            $section = StudentSection::where('name',$value[0]['Section'])->first();
            $teacher = User::where('name',$value[0]['Teacher Name'])->first();
            if(!$teacher){
                $teacher = User::create([
                    'name'=>$value[0]['Teacher Name'],
                    'email'=>str_replace(' ','',$value[0]['Teacher Name']).'@test.com',
                    'password'=>bcrypt('password'),
                    'desgination'=>'Teacher',
                    'title'=>'Teacher',
                    'department_id'=>1,
                    'role'=>'Teacher',
                    'phone'=>'11211212'
                ]);
            }
            $Student =Student::create([
                'student_id'=>$key,
                'first_name'=>$name[0],
                'last_name'=>$name[1]??'',
                'class_id'=>$class->id,
                'section_id'=>$section->id
            ]);
            foreach ($value as $k => $v) {
                $subject = Subject::where('name',$value[0]['Subject'])->first();
                StudentSubject::create([
                    'student_id'=>$Student->id,
                    'teacher_id'=>$teacher->id??'0',
                    'class_id'=>$class->id,
                    'subject_id'=>$subject->id,
                    'mode'=>$v['Mode']
                ]);
                // echo '<pre>';
                // print_r($v['Subject']);
                // echo '</pre>';
            }

        }
        return 'Jobi done or what ever';
    }
    public function subjects()
    {
        $subjects = Subject::all();
        return response()->json(['status'=>true,'data'=>$subjects]);
    }

    public function studentClasses()
    {
        $class = StudentClass::all();
        return response()->json(['status'=>true,'data'=>$class]);
    }


    public function sections()
    {
        $section = StudentSection::all();
        return response()->json(['status'=>true,'data'=>$section]);
    }
}
