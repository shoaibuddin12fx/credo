<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AffairReason;

class AffairReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr =[
            [
                'name'=>'WITHOUTID CARD'
            ],
            [
                'name'=>'ABSENTEESIM'
            ],
            [
                'name'=>'IMPROPER UNIFORM'
            ],
            [
                'name'=>'TEACHER COMPLAIN'
            ],
            [
                'name'=>'ARRIVAL'
            ],
            [
                'name'=>'DESCIPLINE ISSUES'
            ],
            [
                'name'=>'BUNKED'
            ],
            [
                'name'=>'EXCUSED FOR LEAVE'
            ],
            [
                'name'=>'EARLY LEAVE'
            ],
            [
                'name'=>'OTHERS'
            ]
        ];
        AffairReason::insert($arr);
    }
}
