<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table='students';
    protected $fillable =[
        'student_id',
        'first_name',
        'last_name',
        'class_id',
        'section_id'
    ];
    /**
     * Get all of the subjects for the Student
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subjects()
    {
        return $this->hasMany(StudentSubject::class, 'student_id', 'id');
    }
    /**
     * Get the section associated with the Student
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function studentsection()
    {
        return $this->hasOne(StudentSection::class, 'id', 'section_id');
    }
    /**
     * Get the classname associated with the StudentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function studentclass()
    {
        return $this->hasOne(StudentClass::class, 'id', 'class_id');
    }
}
