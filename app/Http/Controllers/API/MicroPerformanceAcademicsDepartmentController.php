<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MicroPerformanceAcademicsDepartment;
use App\Models\MicroPerformanceAcademicsDepartmentSubject;
use Illuminate\Support\Facades\Validator;

class MicroPerformanceAcademicsDepartmentController extends Controller
{
    public function index()
    {
        $performances = MicroPerformanceAcademicsDepartment::with('subjects.subject.subject','user')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $performances]);
    }
    public function show($id)
    {
        $performance = MicroPerformanceAcademicsDepartment::with('subjects.subject.subject','user')->find($id);
        return response()->json(['status' => true, 'data' => $performance]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_id'=>'required',
            'user_id'=>'required',
            'monthly_test_no'=>'required',
            'month'=>'required',
            'year'=>'required',
            'group'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = MicroPerformanceAcademicsDepartment::create($request->all());
        $arr = [];
        if (is_array($request->subjects)) {
            foreach ($request->subjects as $key => $value) {
                $value = (object)$value;
                $arr[] = [
                    'micro_performance_academics_department_id' => $form->id,
                    'student_subject_id'=>$value->student_subject_id,
                    'test_percentage'=>$value->test_percentage,
                    'question_1'=>$value->question_1,
                    'question_2'=>$value->question_2,
                    'question_3'=>$value->question_3,
                    'question_4'=>$value->question_4,
                    'question_5'=>$value->question_5,
                    'question_6'=>$value->question_6,
                    'question_7'=>$value->question_7,
                    'teacher_remarks'=>$value->teacher_remarks,
                    'academics_depart_remarks'=>$value->academics_depart_remarks
                ];
            }
        }

        MicroPerformanceAcademicsDepartmentSubject::insert($arr);
        return response()->json(['status' => true, 'message' => "Form submission successful..."]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'student_id'=>'required',
            'user_id'=>'required',
            'monthly_test_no'=>'required',
            'month'=>'required',
            'year'=>'required',
            'group'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = MicroPerformanceAcademicsDepartment::find($id)->update($request->all());

        $arr = [];
        if (is_array($request->subjects)) {
            foreach ($request->subjects as $key => $value) {
                $value = (object)$value;
                $arr[] = [
                    'micro_performance_academics_department_id' => $id,
                    'student_subject_id'=>$value->student_subject_id,
                    'test_percentage'=>$value->test_percentage,
                    'question_1'=>$value->question_1,
                    'question_2'=>$value->question_2,
                    'question_3'=>$value->question_3,
                    'question_4'=>$value->question_4,
                    'question_5'=>$value->question_5,
                    'question_6'=>$value->question_6,
                    'question_7'=>$value->question_7,
                    'teacher_remarks'=>$value->teacher_remarks,
                    'academics_depart_remarks'=>$value->academics_depart_remarks
                ];
            }
        }
        MicroPerformanceAcademicsDepartmentSubject::where('micro_performance_academics_department_id',$id)->delete();
        MicroPerformanceAcademicsDepartmentSubject::insert($arr);

        return response()->json(['status' => true, 'message' => "Form submission updated successful..."]);
    }
    public function getRecordsByStudentId($studentId)
    {
        $performances = MicroPerformanceAcademicsDepartment::where('student_id',$studentId)->with('subjects.subject.subject','user')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $performances]);
    }
    public function ApproveReject(Request $request,$id)
    {
        $performance = MicroPerformanceAcademicsDepartment::find($id);
        if($performance) {
            $performance->update(['approved'=>$request->approved]);
            return response()->json(['status' => true, 'message' =>'Status updated successfully...']);
        }
        return response()->json(['status' => false, 'message' =>'Invalid Id...']);
    }
}
