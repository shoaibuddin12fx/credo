<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MicroPerformanceCounsellingDepartment extends Model
{
    use HasFactory;
    protected $table ='micro_performance_counselling_departments';

    protected $fillable =[
        'micro_performance_academics_department_id',
        'student_id',
        'approved'
    ];

    /**
     * Get all of the subjects for the MicroPerformanceAcademicsDepartment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subjects()
    {
        return $this->hasMany(MicroPerformanceCounsellingDepartmentSubject::class, 'micro_performance_counselling_department_id', 'id');
    }
    /**
     * Get the academic associated with the MicroPerformanceCounsellingDepartment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function academic()
    {
        return $this->hasOne(MicroPerformanceAcademicsDepartment::class, 'id', 'micro_performance_academics_department_id');
    }
}
