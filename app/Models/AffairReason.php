<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairReason extends Model
{
    use HasFactory;
    protected $table='affair_reasons';
    protected $fillable =[
        'name'
    ];
}
