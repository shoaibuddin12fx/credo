<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\StudentAffairForm;
use App\Models\StudentAffairReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentAffairsController extends Controller
{
    public function index()
    {
        $affairs = StudentAffairForm::with('reasons.reason')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $affairs]);
    }
    public function show($id)
    {
        $affairs = StudentAffairForm::with('reasons.reason')->find($id);
        return response()->json(['status' => true, 'data' => $affairs]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_id' => 'required',
            'name' => 'required',
            'contact' => 'required',
            'counsellor_alloted' => 'required',
            'reason_student' => 'required',
            'student_remarks' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = StudentAffairForm::create($request->all());
        $arr = [];
        if (is_array($request->reasons)) {
            foreach ($request->reasons as $key => $value) {
                $arr[] = [
                    'student_affair_form_id' => $form->id,
                    'affair_reason_id' => $value
                ];
            }
        }

        StudentAffairReason::insert($arr);
        return response()->json(['status' => true, 'message' => "Form submission successful..."]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'student_id' => 'required',
            'name' => 'required',
            'contact' => 'required',
            'counsellor_alloted' => 'required',
            'reason_student' => 'required',
            'student_remarks' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = StudentAffairForm::find($id)->update($request->all());

        $arr = [];
        if (is_array($request->reasons)) {
            foreach ($request->reasons as $key => $value) {
                $arr[] = [
                    'student_affair_form_id' => $id,
                    'affair_reason_id' => $value
                ];
            }
        }
        StudentAffairReason::where('student_affair_form_id', $id)->delete();
        StudentAffairReason::insert($arr);

        return response()->json(['status' => true, 'message' => "Form submission updated successful..."]);
    }
    public function studentAffairsData($studentId)
    {
        $affairs = StudentAffairForm::where('student_id', $studentId)->with('reasons.reason')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $affairs]);
    }
    public function ApproveReject(Request $request, $id)
    {
        $affair =StudentAffairForm::find($id);
        if ($affair) {
            $affair->update(['approved'=>$request->approved]);
            return response()->json(['status' => true, 'message' =>'Status updated successfully...']);
        }
        return response()->json(['status' => false, 'message' =>'Invalid Id...']);
    }
}
