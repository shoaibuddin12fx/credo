<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $arr = [
            [
                'name'=>'Admin',
                'email'=>'admin@admin.com',
                'password'=>bcrypt('password'),
                'desgination'=>'Admin',
                'title'=>'Admin',
                'department_id'=>'4',
                'role'=>'Admin',
                'phone'=>'1224244424'
            ],
            [
                'name'=>'Parker Shahnawaz ',
                'email'=>'parker.edu.pk@gmail.com',
                'password'=>bcrypt('password'),
                'desgination'=>'VP',
                'title'=>'VP',
                'department_id'=>'4',
                'role'=>'Admin',
                'phone'=>'03363700700'
            ],
            [
                'name'=>'Yousuf Zahid',
                'email'=>'yousuf.zahid@credo.edu.pk',
                'password'=>bcrypt('password'),
                'desgination'=>'Head',
                'title'=>'Head',
                'department_id'=>'3',
                'role'=>'Teacher',
                'phone'=>'03332899938'
            ],
            [
                'name'=>'Hadia Tariq',
                'email'=>'hadia.tariq@credo.edu.pk',
                'password'=>bcrypt('password'),
                'desgination'=>'Teacher',
                'title'=>'Teacher',
                'department_id'=>'3',
                'role'=>'Teacher',
                'phone'=>'03002001279'
            ],
            [
                'name'=>'Sahrish Fazal',
                'email'=>'sahrish.fazal@credo.edu.pk',
                'password'=>bcrypt('password'),
                'desgination'=>'Teacher',
                'title'=>'Teacher',
                'department_id'=>'3',
                'role'=>'Teacher',
                'phone'=>'03039700108'
            ],
            [
                'name'=>'Anum Akhter',
                'email'=>'anum.akhtar@credo.edu.pk',
                'password'=>bcrypt('password'),
                'desgination'=>'Head',
                'title'=>'Head',
                'department_id'=>'2',
                'role'=>'Teacher',
                'phone'=>'03333018358'
            ],
            [
                'name'=>'Saghar Babar',
                'email'=>'waqassaghar@gmail.com',
                'password'=>bcrypt('password'),
                'desgination'=>'Teacher',
                'title'=>'Teacher',
                'department_id'=>'2',
                'role'=>'Teacher',
                'phone'=>'03433204880'
            ],
            [
                'name'=>'Muhammad Danish',
                'email'=>'dani.md365@gmail.com',
                'password'=>bcrypt('password'),
                'desgination'=>'Teacher',
                'title'=>'Teacher',
                'department_id'=>'1',
                'role'=>'Teacher',
                'phone'=>'03430212616'
            ],
            [
                'name'=>'Hassan Ahmed Siddiqui',
                'email'=>'hassan.siddiqui511@gmail.com',
                'password'=>bcrypt('password'),
                'desgination'=>'Teacher',
                'title'=>'Teacher',
                'department_id'=>'1',
                'role'=>'Teacher',
                'phone'=>'03152735969'
            ],
            [
                'name'=>'Heeba Zehra',
                'email'=>'heebazehra@gmail.com',
                'password'=>bcrypt('password'),
                'desgination'=>'Head',
                'title'=>'Head',
                'department_id'=>'1',
                'role'=>'Teacher',
                'phone'=>'03322106213'
            ]
        ];

        User::insert($arr);
    }
}
