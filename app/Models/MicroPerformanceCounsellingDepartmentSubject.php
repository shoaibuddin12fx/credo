<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MicroPerformanceCounsellingDepartmentSubject extends Model
{
    use HasFactory;
    protected $table ='micro_performance_counselling_department_subjects';
    protected $fillable =[
        'micro_performance_counselling_department_id',
        'student_subject_id',
        'reason_for_under_performance',
        'weekly_counselling',
        'meet_the_teacher',
        'peer_tutoring',
        'meet_the_parents',
        'associate_teacher',
        'recommendations',
        'first_follow_up',
        'first_follow_up_out_come',
        'second_follow_up',
        'second_follow_up_out_come',
        'third_follow_up',
        'third_follow_up_out_come',
        'final_follow_up',
        'final_follow_up_out_come',
    ];
    /**
     * Get the subject associated with the MicroPerformanceCounsellingDepartmentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne(StudentSubject::class, 'id', 'student_subject_id');
    }
}
