<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);
        if($validator->fails()){
             return response()->json(['status'=>false,'message'=>$validator->errors()]);
        }
        if(Auth::attempt(['email' =>  $request->email, 'password' => $request->password])){

            $user = Auth::user();
            $token = $user->createToken('credo')->accessToken;
            return response()->json(['status'=>true,'data'=> $user, 'token' => $token]);
        }
        return response()->json(['status'=>false,'message'=>'Incorrect email or password']);
    }
    public function users($role)
    {
        if($role==='Admin'){
            $users = User::where('role','!=','Admin')->paginate(15);
            return response()->json(['status'=>true,'data'=>$users]);
        }
        return response()->json(['status'=>false,'message'=>"You don't have permissions to access"]);

    }
}
