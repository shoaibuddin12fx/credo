<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAffairReason extends Model
{
    use HasFactory;
    protected $table='student_affair_reasons';
    protected $fillable =[
        'student_affair_form_id',
        'affair_reason_id'
    ];

    /**
     * Get the reason associated with the StudentAffairReason
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reason()
    {
        return $this->hasOne(AffairReason::class, 'id', 'affair_reason_id');
    }
}
