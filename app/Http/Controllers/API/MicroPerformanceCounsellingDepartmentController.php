<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MicroPerformanceCounsellingDepartment;
use App\Models\MicroPerformanceCounsellingDepartmentSubject;
use Illuminate\Support\Facades\Validator;

class MicroPerformanceCounsellingDepartmentController extends Controller
{
    public function index()
    {
        $performances = MicroPerformanceCounsellingDepartment::with('subjects.subject.subject','academic.subjects')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $performances]);
    }
    public function show($id)
    {
        $performance = MicroPerformanceCounsellingDepartment::with('subjects.subject.subject','academic.subjects')->find($id);
        return response()->json(['status' => true, 'data' => $performance]);
    }
    public function getRecordsByStudentId($studentId)
    {
        $performances = MicroPerformanceCounsellingDepartment::where('student_id',$studentId)->with('subjects.subject.subject')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $performances]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'micro_performance_academics_department_id'=>'required',
            'student_id'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = MicroPerformanceCounsellingDepartment::create($request->all());
        $arr = [];
        if (is_array($request->subjects)) {
            foreach ($request->subjects as $key => $value) {
                $value = (object)$value;
                $arr[] = [
                    'micro_performance_counselling_department_id' => $form->id,
                    'student_subject_id'=>$value->student_subject_id,
                    'reason_for_under_performance'=>$value->reason_for_under_performance,
                    'weekly_counselling'=>$value->weekly_counselling,
                    'meet_the_teacher'=>$value->meet_the_teacher,
                    'peer_tutoring'=>$value->peer_tutoring,
                    'meet_the_parents'=>$value->meet_the_parents,
                    'associate_teacher'=>$value->associate_teacher,
                    'recommendations'=>$value->recommendations,
                    'first_follow_up'=>$value->first_follow_up,
                    'first_follow_up_out_come'=>$value->first_follow_up_out_come,
                    'second_follow_up'=>$value->second_follow_up,
                    'second_follow_up_out_come'=>$value->second_follow_up_out_come,
                    'third_follow_up'=>$value->third_follow_up,
                    'third_follow_up_out_come'=>$value->third_follow_up_out_come,
                    'final_follow_up'=>$value->final_follow_up,
                    'final_follow_up_out_come'=>$value->final_follow_up_out_come,
                ];
            }
        }

        MicroPerformanceCounsellingDepartmentSubject::insert($arr);
        return response()->json(['status' => true, 'message' => "Form submission successful..."]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'micro_performance_academics_department_id'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()]);
        }
        $form = MicroPerformanceCounsellingDepartment::find($id)->update($request->all());

        $arr = [];
        if (is_array($request->subjects)) {
            foreach ($request->subjects as $key => $value) {
                $value = (object)$value;
                $arr[] = [
                    'micro_performance_counselling_department_id' => $id,
                    'student_subject_id'=>$value->student_subject_id,
                    'reason_for_under_performance'=>$value->reason_for_under_performance,
                    'weekly_counselling'=>$value->weekly_counselling,
                    'meet_the_teacher'=>$value->meet_the_teacher,
                    'peer_tutoring'=>$value->peer_tutoring,
                    'meet_the_parents'=>$value->meet_the_parents,
                    'associate_teacher'=>$value->associate_teacher,
                    'recommendations'=>$value->recommendations,
                    'first_follow_up'=>$value->first_follow_up,
                    'first_follow_up_out_come'=>$value->first_follow_up_out_come,
                    'second_follow_up'=>$value->second_follow_up,
                    'second_follow_up_out_come'=>$value->second_follow_up_out_come,
                    'third_follow_up'=>$value->third_follow_up,
                    'third_follow_up_out_come'=>$value->third_follow_up_out_come,
                    'final_follow_up'=>$value->final_follow_up,
                    'final_follow_up_out_come'=>$value->final_follow_up_out_come,
                ];
            }
        }
        MicroPerformanceCounsellingDepartmentSubject::where('micro_performance_counselling_department_id',$id)->delete();
        MicroPerformanceCounsellingDepartmentSubject::insert($arr);

        return response()->json(['status' => true, 'message' => "Form submission updated successful..."]);
    }
    public function ApproveReject(Request $request,$id)
    {
        $performance = MicroPerformanceCounsellingDepartment::find($id);
        if($performance) {
            $performance->update(['approved'=>$request->approved]);
            return response()->json(['status' => true, 'message' =>'Status updated successfully...']);
        }
        return response()->json(['status' => false, 'message' =>'Invalid Id...']);
    }
}
