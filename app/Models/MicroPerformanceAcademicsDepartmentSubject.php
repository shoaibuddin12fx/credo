<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MicroPerformanceAcademicsDepartmentSubject extends Model
{
    use HasFactory;

    protected $table ='micro_performance_academics_department_subjects';

    protected $fillable =[
        'micro_performance_academics_department_id',
        'student_subject_id',
        'test_percentage',
        'question_1',
        'question_2',
        'question_3',
        'question_4',
        'question_5',
        'question_6',
        'question_7',
        'teacher_remarks',
        'academics_depart_remarks'
    ];

     /**
     * Get the subject associated with the MicroPerformanceCounsellingDepartmentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne(StudentSubject::class, 'id', 'student_subject_id');
    }
}
