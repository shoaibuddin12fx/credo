<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MicroPerformanceAcademicsDepartment extends Model
{
    use HasFactory;

    protected  $table ='micro_performance_academics_departments';

    protected $fillable =[
        'student_id',
        'user_id',
        'monthly_test_no',
        'month',
        'year',
        'group',
        'approved'
    ];
    /**
     * Get all of the subjects for the MicroPerformanceAcademicsDepartment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subjects()
    {
        return $this->hasMany(MicroPerformanceAcademicsDepartmentSubject::class, 'micro_performance_academics_department_id', 'id');
    }
    /**
     * Get the user associated with the MicroPerformanceAcademicsDepartment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
