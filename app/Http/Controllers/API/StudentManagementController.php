<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;

class StudentManagementController extends Controller
{

    public function index()
    {
        $students = Student::with('studentclass', 'studentsection', 'subjects.subject')->latest()->paginate(15);
        return response()->json(['status' => true, 'data' => $students]);
    }

    public function show($id)
    {
        $student = Student::with('subjects.subject', 'studentsection', 'studentclass', 'subjects.teacher')->find($id);
        return response()->json(['status' => true, 'data' => $student]);
    }

    public function profile($id)
    {
        $student = Student::find($id);
        return response()->json(['status' => true, 'data' => $student]);
    }

    public function setPicture(Request $request)
    {

        $data = $request->all();
//        $profile_image = $request->image;

        if ($request->has('image') && !empty($data["image"])) {

            dd(base64_decode($data["image"], true));

//            if (base64_decode($request->image, true) != false) {
//                // Check if folder exists
//                if (!is_dir(public_path('images/profile_images'))) {
//                    mkdir(public_path('images/profile_images'), 0777, true);
//                }
//                $profile_image = $this->delete_all_between("data:image", "base64,", $request->image);
//                $image = base64_decode($profile_image);
//                $imageName = 'profile_' . md5(uniqid()) . '.' . 'png';
//                $path = public_path("images/profile_images/{$imageName}");
//                file_put_contents($path, $image);
//                $data['image'] = $imageName;
//            }
        }

    }

    public function setColor(Request $request)
    {

    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $data["student_id"];

        $res = Student::find($id)->where('student_id', $id)->update($data);

        if ($res) {
            return [
                'result' => "Success"
            ];
        }
    }

    private function delete_all_between(string $beginning, string $end, string $string)
    {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }
        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
        return str_replace($textToDelete, '', $string);
    }

}
