<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAffairForm extends Model
{
    use HasFactory;
    protected $table='student_affair_forms';
    protected $fillable =[
        'student_id',
        'name',
        'contact',
        'counsellor_alloted',
        'reason_student',
        'student_remarks',
        'parent_concern',
        'approved'
    ];

    /**
     * Get all of the comments for the StudentAffairForm
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reasons()
    {
        return $this->hasMany(StudentAffairReason::class, 'student_affair_form_id', 'id');
    }
}
