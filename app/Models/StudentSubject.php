<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSubject extends Model
{
    use HasFactory;
    protected $table='student_subjects';
    protected $fillable =[
        'student_id',
        'teacher_id',
        'class_id',
        'subject_id',
        'mode'
    ];
    /**
     * Get the subject associated with the StudentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }
    /**
     * Get the teacher associated with the StudentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teacher()
    {
        return $this->hasOne(User::class, 'id', 'teacher_id');
    }
    /**
     * Get the classname associated with the StudentSubject
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function studentclass()
    {
        return $this->hasOne(StudentClass::class, 'id', 'class_id');
    }
}
